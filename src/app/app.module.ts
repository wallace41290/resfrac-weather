import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AuthModule } from 'angular-auth-oidc-client';
import { NgxElectronModule } from 'ngx-electron';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent, CoreModule } from './core';
import { AuthEffects } from './core/effects';
import { HomeModule } from './home';
import { metaReducers, ROOT_REDUCERS } from './reducers';
import { SettingsModule } from './settings';

@NgModule({
  imports: [
    AppRoutingModule,
    AuthModule.forRoot({
      config: {
        authority: 'https://dev-7pt3rl6h.us.auth0.com',
        redirectUrl: 'http://localhost/callback',
        clientId: 'I6DY2MwPZUk1fK1gNzWvMMTfGhwTdNDr',
        scope: 'openid profile offline_access',
        responseType: 'code',
        silentRenew: true,
        useRefreshToken: true,
      },
    }),
    BrowserAnimationsModule,
    BrowserModule,
    CoreModule,
    HomeModule,
    NgxElectronModule,
    SettingsModule,
    StoreModule.forRoot(ROOT_REDUCERS, {
      metaReducers,
      runtimeChecks: {
        // strictStateImmutability and strictActionImmutability are enabled by default
        strictStateSerializability: true,
        strictActionSerializability: true,
        strictActionWithinNgZone: true,
        strictActionTypeUniqueness: true,
      },
    }),
    StoreDevtoolsModule.instrument({
      name: 'ResFrac Weather App',
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([AuthEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
