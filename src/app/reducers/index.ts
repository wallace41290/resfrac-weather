import { InjectionToken } from '@angular/core';

import { Action, ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';

import { environment } from '../../environments/environment';
import * as fromAuth from '../core/reducers/auth.reducer';
import * as fromLayout from '../core/reducers/layout.reducer';

export interface State {
  [fromLayout.layoutFeatureKey]: fromLayout.State;
  [fromAuth.authFeatureKey]: fromAuth.State;
}

export const ROOT_REDUCERS = new InjectionToken<ActionReducerMap<State, Action>>('Root reducers token', {
  factory: () => ({
    [fromLayout.layoutFeatureKey]: fromLayout.reducer,
    [fromAuth.authFeatureKey]: fromAuth.reducer,
  }),
});

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return result;
  };
}

export const metaReducers: MetaReducer<State>[] = !environment.production ? [logger] : [];

export const selectLayoutState = createFeatureSelector<fromLayout.State>(fromLayout.layoutFeatureKey);
export const selectLayoutDarkTheme = createSelector(selectLayoutState, fromLayout.selectDarkTheme);

export const selectAuthState = createFeatureSelector<fromAuth.State>(fromAuth.authFeatureKey);
export const selectAuthenticated = createSelector(selectAuthState, fromAuth.selectAuthenticated);
