import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SettingsPageComponent } from './containers';
import { SettingsRoutingModule } from './settings-routing.module';

@NgModule({
  imports: [CommonModule, SettingsRoutingModule],
  declarations: [SettingsPageComponent],
})
export class SettingsModule {}
