import { createReducer, on } from '@ngrx/store';

import { HomePageActions } from '../actions';

export const weatherFeatureKey = 'weather';

export interface State {
  fetching: boolean;
  query: unknown; // TODO model
}

const initialState: State = {
  fetching: false,
  query: undefined,
};

export const reducer = createReducer(
  initialState,
  on(HomePageActions.fetchWeather, (state, { query }) => ({
    ...state,
    query,
    fetching: true,
  }))
);

export const getFetching = (state: State) => state.fetching;
export const getQuery = (state: State) => state.query;
