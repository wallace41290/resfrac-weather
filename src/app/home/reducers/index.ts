import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromRoot from '../../reducers';
import * as fromWeather from './weather.reducer';

export const homeFeatureKey = 'home';

export interface HomeState {
  [fromWeather.weatherFeatureKey]: fromWeather.State;
}

export interface State extends fromRoot.State {
  [homeFeatureKey]: HomeState;
}

/** Provide reducer in AoT-compilation happy way */
export function reducers(state: HomeState | undefined, action: Action) {
  return combineReducers({
    [fromWeather.weatherFeatureKey]: fromWeather.reducer,
  })(state, action);
}

export const selectHomeState = createFeatureSelector<HomeState>(homeFeatureKey);

export const selectWeatherState = createSelector(selectHomeState, (state) => state.weather);

export const selectWeatherQuery = createSelector(selectWeatherState, fromWeather.getQuery);
export const selectWeatherFetching = createSelector(selectWeatherState, fromWeather.getFetching);
