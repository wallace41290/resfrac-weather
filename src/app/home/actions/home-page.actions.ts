import { createAction, props } from '@ngrx/store';

export const fetchWeather = createAction(
  '[Home Page] Fetch Weather',
  props<{ query: unknown }>() // TODO model
);
