import { Component } from '@angular/core';

import { Store } from '@ngrx/store';

import { AuthActions } from '../../core/actions';
import * as fromRoot from '../../reducers';

@Component({
  selector: 'app-home',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {
  /** Whether the user is logged in. */
  authenticated$ = this.store.select(fromRoot.selectAuthenticated);

  constructor(private store: Store) {}

  login() {
    this.store.dispatch(AuthActions.login());
  }
}
