import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { WeatherIllustrationComponent } from './components';
import { HomePageComponent } from './containers';
import { WeatherEffects } from './effects';
import { HomeRoutingModule } from './home-routing.module';
import * as fromHome from './reducers';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    StoreModule.forFeature(fromHome.homeFeatureKey, fromHome.reducers),

    EffectsModule.forFeature([WeatherEffects]),
  ],
  declarations: [HomePageComponent, WeatherIllustrationComponent],
})
export class HomeModule {}
