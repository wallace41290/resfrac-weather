export interface HasTheme {
  dark: boolean;
}
