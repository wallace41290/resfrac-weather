export interface HasWeather {
  apiKey: string;
  apiUrl: string;
}
