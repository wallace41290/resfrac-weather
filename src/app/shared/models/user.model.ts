import { HasTheme } from './has-theme.model';
import { HasWeather } from './has-weather.model';

export interface User extends HasWeather, HasTheme {
  email: string;
  firstName: string;
  id: string;
  lastName: string;
  picture: string;
}
