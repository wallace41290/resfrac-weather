import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';

import { AppComponent } from './containers';

@NgModule({
  imports: [CommonModule, FlexLayoutModule, MatButtonModule, MatIconModule, MatToolbarModule, MatTooltipModule, RouterModule],
  providers: [],
  declarations: [AppComponent],
  exports: [AppComponent],
})
export class CoreModule {}
