import { Injectable } from '@angular/core';

import { OidcSecurityService } from 'angular-auth-oidc-client';
import { ElectronService } from 'ngx-electron';

export function authFactory(electronService: ElectronService, oidcSecurityService: OidcSecurityService) {
  if (electronService.isElectronApp) {
    return new DesktopAuthService(oidcSecurityService);
  }

  return new WebAuthService(oidcSecurityService);
}

@Injectable({
  providedIn: 'root',
  useFactory: authFactory,
  deps: [ElectronService, OidcSecurityService],
})
export abstract class AuthBaseService {
  modal: Window | undefined;

  constructor(public oidcSecurityService: OidcSecurityService) {}

  get isLoggedIn() {
    return this.oidcSecurityService.isAuthenticated$;
  }

  get token() {
    return this.oidcSecurityService.getAccessToken();
  }

  get userData$() {
    return this.oidcSecurityService.userData$;
  }

  checkAuth(url?: string) {
    if (this.modal) {
      this.modal.close();
    }

    return this.oidcSecurityService.checkAuth(url);
  }

  abstract doLogin(): void;

  signOut() {
    return this.oidcSecurityService.logoffAndRevokeTokens();
  }
}

export class DesktopAuthService extends AuthBaseService {
  doLogin() {
    // const urlHandler = (authUrl) => {
    //   this.modal = window.open(authUrl, '_blank', 'nodeIntegration=no');
    // };

    this.oidcSecurityService.authorizeWithPopUp();
  }
}

export class WebAuthService extends AuthBaseService {
  doLogin() {
    this.oidcSecurityService.authorize();
  }
}
