import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

import { Store } from '@ngrx/store';

import { tap } from 'rxjs/operators';

import { ElectronService } from 'ngx-electron';

import { environment } from '../../../environments/environment';
import * as fromRoot from '../../reducers';
import { AuthActions, LayoutActions } from '../actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  @HostBinding('class.dark-mode') bindAppDarkModeCssClass = false;

  /** Whether dark mode is enabled, defaults to false. */
  darkMode$ = this.store.select(fromRoot.selectLayoutDarkTheme).pipe(tap((darkTheme) => (this.bindAppDarkModeCssClass = darkTheme)));
  /** Whether the user is logged in. */
  authenticated$ = this.store.select(fromRoot.selectAuthenticated);

  constructor(private electronService: ElectronService, private store: Store) {
    console.log('environment', environment);

    if (electronService.isElectronApp) {
      console.log(process.env);
      console.log('Run in electron');
      console.log('Electron ipcRenderer', this.electronService.ipcRenderer);
    } else {
      console.log('Run in browser');
    }
  }

  login() {
    this.store.dispatch(AuthActions.login());
  }

  logout() {
    this.store.dispatch(AuthActions.logout());
  }

  switchTheme() {
    this.store.dispatch(LayoutActions.toggleTheme());
  }
}
