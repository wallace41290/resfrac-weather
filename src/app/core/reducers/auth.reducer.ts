import { createReducer, on } from '@ngrx/store';

import { AuthApiActions } from '../actions';

export const authFeatureKey = 'auth';

export interface State {
  authenticated: boolean;
}

const initialState: State = {
  authenticated: false,
};

export const reducer = createReducer(
  initialState,
  on(AuthApiActions.loginSuccess, (state) => ({ authenticated: true })),
  on(AuthApiActions.logoutSuccess, AuthApiActions.loginFailure, (state) => ({ authenticated: false }))
);

export const selectAuthenticated = (state: State) => state.authenticated;
