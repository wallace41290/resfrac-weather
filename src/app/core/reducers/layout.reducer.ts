import { createReducer, on } from '@ngrx/store';

import { LayoutActions } from '../actions';

export const layoutFeatureKey = 'layout';

export interface State {
  darkTheme: boolean;
}

const initialState: State = {
  darkTheme: false,
};

export const reducer = createReducer(
  initialState,
  on(LayoutActions.toggleTheme, (state) => ({ darkTheme: !state.darkTheme }))
);

export const selectDarkTheme = (state: State) => state.darkTheme;
