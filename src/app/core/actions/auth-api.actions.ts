import { createAction, props } from '@ngrx/store';

export const loginFailure = createAction('[Auth/API] Login Failure', props<{ error: unknown }>());
export const loginSuccess = createAction('[Auth/API] Login Success', props<{ user: unknown }>());

export const logoutFailure = createAction('[Auth/API] Logout Failure', props<{ error: unknown }>());
export const logoutSuccess = createAction('[Auth/API] Logout Success');
