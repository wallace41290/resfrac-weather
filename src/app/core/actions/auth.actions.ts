import { createAction } from '@ngrx/store';

export const logout = createAction('[Auth] Logout');
export const login = createAction('[Auth] Login');
