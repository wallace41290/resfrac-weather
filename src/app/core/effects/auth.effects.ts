import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';

import { map } from 'rxjs/operators';

import { AuthActions, AuthApiActions } from '../actions';
import { AuthBaseService } from '../services';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      map(() => {
        // Temporary just to quickly test the authentication
        // FIXME: actually wait for the login response before returning the success/fail action
        // this.authService.doLogin();
        return AuthApiActions.loginSuccess({ user: {} });
      })
    )
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      map(() => {
        // Temporary just to quickly test the authentication
        // FIXME: actually wait for the logout response before returning the success/fail action
        // this.authService.signOut();
        return AuthApiActions.logoutSuccess();
      })
    )
  );

  constructor(private actions$: Actions, private authService: AuthBaseService) {}
}
